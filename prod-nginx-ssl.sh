#!/bin/bash

sudo apt-get update;
sudo apt-get install nginx;

sudo bash -c "cat > /etc/nginx/sites-available/diana.conf <<EOF
server {
    listen 80;
    
    server_name dianalearning.site;
    
    location / {
        proxy_pass http://35.194.17.57:8081;
        proxy_set_header Host \$host;
        proxy_set_header x-Real-IP \$remote__addr;
    }
    
    location /users {
        proxy_pass http://35.194.17.57:8080;
        proxy_set_header Host \$host;
        proxy_set_header x-Real-IP \$remote__addr;
    }
}
EOF";

sudo ln -s /etc/nginx/sites-available/diana.conf /etc/nginx/sites-enabled/;
sudo apt-get install certbot python3-certbot-nginx;
sudo certbot --nginx -d dianalearning.site;
sudo service nginx restart;

#!/bin/bash

sudo apt-get update;
sudo apt-get install nginx;

sudo bash -c "cat > /etc/nginx/sites-available/dev-diana.conf <<EOF
server {
    listen 80;
    
    server_name dev.dianalearning.site;
    
    location / {
        proxy_pass http://35.185.186.142:8081;
        proxy_set_header Host \$host;
        proxy_set_header x-Real-IP \$remote_addr;
    }
    
    location /users {
        proxy_pass http://35.185.186.142:8080;
        proxy_set_header Host \$host;
        proxy_set_header x-Real-IP \$remote_addr;
    }
}
EOF";

sudo ln -s /etc/nginx/sites-available/dev-diana.conf /etc/nginx/sites-enabled/;
sudo apt-get install certbot python3-certbot-nginx;
sudo certbot --nginx -d dev.dianalearning.site;
sudo service nginx restart;
